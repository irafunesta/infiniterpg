//#pragma once

#ifndef _infrpg_status_h_
#define _infrpg_status_h_

#include <iostream>

class FGame;

struct State
{
public:
	State();
	~State();

	virtual void Update(FGame& game) = 0;
	virtual void Enter() = 0;
	virtual void Leave() = 0;

	void Clear();
	void Continue(std::string message = "Press anykey to continue");
};

#endif

