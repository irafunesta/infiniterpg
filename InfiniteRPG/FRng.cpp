#include "FRng.h"

FRng::FRng()
{
	RandomGenerator.seed(time(NULL)); //create the generator and make the seed ?
	return;
}

int FRng::CreateRandomNumber(int Min, int Max)
{
	std::uniform_int_distribution<int> IntDice(Min, Max);
	return IntDice(RandomGenerator);
}
