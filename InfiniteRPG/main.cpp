//This is the entry point of the game
/*
	Infinite RPG v0.1.2
	A Game where you fight a infinite series of monster.
	The game end whit your defeat, the level you have reached
	is your score.
*/
//#include "FActor.h" move to the FGame
//Mine
#include "FGame.h"
#include "MainMenuState.h"
#include "FightState.h"
//Libs
//STD
#include <iostream>
#include <string>

void PrintIntro();

int main()
{	
	FGame Game{};
	/*PPP Have a game class 
		Start the game with a quick intro
		Make the main loop for playing
		Exit when the game need to end
	*/

	//Set up states
	//State Intro = State{};
	MainMenuState Menu = MainMenuState{};
	FightState Fight = FightState{};
	//State Fight = State{};

	//Load items
	Game.LoadItems();

	PrintIntro();
	
	Game.StartGame();
	Game.state_list[0] = &Menu;
	Game.state_list[1] = &Fight;
	Game.SetCurrentStatus(FGame::States::MAIN_MENU);
	
	while (!Game.IsGameEnded())
	{		
		Game.Update(Game);
	}

	Game.ExitGame();
	system("PAUSE"); //Avoid console closing before time
	return 0;
}

//Can use only one instruction and a variable;
void PrintIntro() 
{
	std::cout << std::endl;
	std::cout << "              Welcome to Infinite RPG                 " << std::endl;
	std::cout << " A Game where you fight a infinite serire of monsters." << std::endl;
	std::cout << "           The game end with your defeat.             " << std::endl;
	std::cout << "     The level you have reached, is your score.       " << std::endl;
	std::cout << "                  by Guidi Simone";	
}


