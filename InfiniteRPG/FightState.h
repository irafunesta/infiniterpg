
#ifndef _infrpg_fightstate_
#define _infrpg_fightstate_

#include "State.h"

class FGame;

class FightState :
	public State
{
public:
	FightState();
	~FightState();

	virtual void Update(FGame& game);
	virtual void Enter();
	virtual void Leave();
};

#endif // !_infrpg_fightstate_
