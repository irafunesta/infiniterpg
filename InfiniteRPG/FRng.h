
#ifndef _infrpg_frng_h_
#define _infrpg_frng_h_

#include <ctime>
#include <random>

class FRng
{
public:
	FRng();
	std::mt19937 RandomGenerator;
	int CreateRandomNumber(int Min, int Max);
};

#endif