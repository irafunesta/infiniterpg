#include "MainMenuState.h"
#include "FGame.h"

#include <string>
#include <iostream>


MainMenuState::MainMenuState()
{
}


MainMenuState::~MainMenuState()
{
}

void MainMenuState::Update(FGame& game)
{
	//Print the main menu
	//Check for inputs
	std::system("cls");
	std::cout << " ---------- Player Status ---------- " << std::endl;
	game.PrintPlayerStatus();

	std::cout << " ----------------------------------- " << std::endl;
	std::cout << " -------- Select an action --------- " << std::endl;
	
	std::cout << "         1 - Next Fight              " << std::endl;
	std::cout << "         2 - Player Status           " << std::endl;
	std::cout << "         3 - Exit Game               " << std::endl;
	std::cout << "         4 - Player Inventory        " << std::endl;


	//Clear();

	int Action = 0;
	Action = std::getchar();
	//Get the user input and expect it to be a number
	switch (Action)
	{
		case '1': //Fight the next enemy
		{
			game.GenerateNextEnemy();
			game.SetCurrentStatus(FGame::States::FIGHT);
			break;
		}
		case '2': //End the game
		{
			game.PrintPlayerStatus();
			Continue();
			break;
		}
		case '3': //Hidden testing purpose case ##Test
		{
			game.bGameFinished = true;
			break;
		}
		case '4': //Hidden testing purpose case ##Test
		{
			game.PrintPlayerInventory();
			Continue();
			break;
		}
		default:
		{
			std::cout << "Invalid option" << std::endl;
			Continue();
			break;
		}
	}
}

void MainMenuState::Enter()
{
	//Print something 
	std::cout << "Enter main menu" << std::endl;
}

void MainMenuState::Leave()
{
	std::cout << "leave main menu" << std::endl;
}
