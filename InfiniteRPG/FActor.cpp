#include "FActor.h"

FActor::FActor()
{
	
}

FActor::FActor(std::string Name, std::string Description, int HitPoints, int Attack, int Armor, int Level)
{
	srand(time(NULL));

	name = Name;
	description = Description;
	hit_points = HitPoints;
	attack = Attack;
	armor = Armor;
	level = Level;
	experience = 0;
	next_level_exp = base_next_level * level + RandGen.CreateRandomNumber(0, 100);
	exp_reward = CalculateExpReward();
}

std::string FActor::GetName() const
{
	return name;
}

std::string FActor::GetDescription() const
{
	return description;
}

int FActor::GetHitPoints() const
{
	return hit_points;
}

int FActor::GetAttack() const
{
	return attack;
}

int FActor::GetArmor() const
{
	return armor;
}
int FActor::GetLevel() const
{
	return level;
}

int FActor::GetExperience() const
{
	return experience;
}

int FActor::GetNextLevelExp() const
{
	return next_level_exp;
}

int FActor::GetExpReward() const
{
	return exp_reward;
}

//Function called every time the player loose life
void FActor::ApplyDamage(int Damage)
{
	hit_points -= Damage;
}

void FActor::AddExperience(int ExpPoint)
{
	experience += ExpPoint;
	if (experience >= next_level_exp) {
		//Level up
		LevelUp();
	}
}

bool FActor::IsDead()
{
	return (hit_points <= 0);
}

void FActor::LevelUp()
{
	//Mantain the experience, and in case trigger multiple levelup
	int OverExp = experience - next_level_exp;
	experience = 0;
	level += 1;
	next_level_exp = base_next_level * level + RandGen.CreateRandomNumber(0, 100);
	//Trigger multiple Levelups in case
	AddExperience(OverExp);	
}

int FActor::CalculateExpReward()
{
	int Exp = 0;
	int HpWeight = 3;
	int AtkWeight = 2;
	int DefWeight = 3;
	int WeightSum = HpWeight + AtkWeight + DefWeight;
	int ActorValue = (hit_points * HpWeight) + (attack * AtkWeight) + (armor * DefWeight);
	Exp = level + ActorValue / 3 + WeightSum;
	return Exp;
}

//Apply damage to an Actor, return the damage dealt
int FActor::Attack(FActor& Other)
{
	//Deal damage to another Actor
	//The Damage is The Attack Value + Random(20) - Other armor;
	//The damage is deal to the other actor
	int DiceRoll = RandGen.CreateRandomNumber(1, 20);
	int Damage = attack + DiceRoll - Other.GetArmor();

	//Damage become negative some times, in that case is 0
	if (Damage < 0) 
	{
		Damage = 0;
	}
	Other.ApplyDamage(Damage);
	return Damage;
}

void FActor::SetHitPoints(int HitPoint)
{
	hit_points = HitPoint;
}

void FActor::SetAttack(int Attack)
{
	attack = Attack;
}

void FActor::SetArmor(int Armor)
{
	armor = Armor;
}

uint32_t FActor::GetCurrency() const
{
	return currency;
}

void FActor::SetCurrency(uint32_t value)
{
	currency = value;
}