#include "State.h"
#include <iostream> //Declared 2 times ?

State::State()
{
}


State::~State()
{
}

void State::Clear()
{
	std::cin.clear();
	std::cin.ignore(99, '\n');
}

void State::Continue(std::string message)
{
	Clear();
	std::cout << message.c_str() << std::endl;
	std::getchar(); //Stop 
}
