#ifndef _infrpg_fgame_h_
#define _infrpg_fgame_h_

#include "State.h" 
#include "FActor.h"
#include "FRng.h"
#include "Item.h"
//class State; //Pointer/refernce to this, use a forward declaration NOT WORKING

#include <map>

class FGame
{
public:
	FGame();
	void StartGame();
	void Update(FGame& game);
	void ExitGame(); //Say thx and end the program
	void EndGame(); //The player has died, show the level and ask to play again
	void Reset();
	bool IsGameEnded() const;
	
	FActor GetPlayer() const;

	State *currentStatus;
	void SetCurrentStatus(int status_ptr);
	void GenerateNextEnemy();
	bool bGameFinished;
	void PrintPlayerStatus();
	void PrintEnemyStatus();
	void Battle();
	void Defend();

	
	static enum States //The varios state of the game
	{
		MAIN_MENU = 0,
		FIGHT = 1
	};

	State* state_list[2];

	void LoadItems();
	std::map<int, Item> item_pool;

	void PrintPlayerInventory();

private:
	
	FActor Player;
	FActor Enemy;
	int GameDifficulty;
	FRng NumGenerator;

	bool CheckAction(int Action); //execute the selected menu action
	void PrintMenu(); // Print the main menu each turn
	void PrintActorStatus(FActor Actor, bool Details = false);
	void PrintBattleResult(int PlayerDamage, int EnemyDamage,int Healing, int TempArmor);
	
	void GeneratePlayer(std::string Name);
	int CalculateEnemyStat(int BaseValue, int EnemyLevel, int GameDifficulty);
	void LevelUpPlayerStats();
	std::string GenerateEnemyName();
};

#endif // !