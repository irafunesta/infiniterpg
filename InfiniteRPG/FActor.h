//#pragma once

#ifndef _infrpg_factor_h_
#define _infrpg_factor_h_

#include "FRng.h"
#include <map>
#include <string>
//Class defining an actor of the game like the Player and the Enemies

class FActor
{
public:
	FActor();
	FActor(std::string Name, std::string Description, 
			int HitPoints, int Attack, int Armor, int Level);
	std::string GetName() const;
	std::string GetDescription() const;
	int GetHitPoints() const;
	int GetAttack() const;
	int GetArmor() const;
	int GetLevel() const;
	int GetExperience() const;
	int GetNextLevelExp() const;
	int GetExpReward() const;
	uint32_t GetCurrency() const;

	int Attack(FActor& Other);

	void SetHitPoints(int HitPoint);
	void SetAttack(int Attack);
	void SetArmor(int Armor);
	void SetCurrency(uint32_t value);

	void ApplyDamage(int Damage);
	void AddExperience(int ExpPoint);
	bool IsDead();

	std::map<int, int> inventory;
private:
	std::string name;
	std::string description;
	int hit_points;
	int attack;
	int armor;
	int level;
	int experience;
	int next_level_exp;
	int exp_reward; //Enemy OOP problem
	int base_next_level = 200;
	uint32_t currency = 0; //Player golds, enemy drop value
	FRng RandGen;

	void LevelUp();
	int CalculateExpReward();
};

#endif