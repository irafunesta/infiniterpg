//Mine
#include "FGame.h"
//3rd party libs
//standard and system
#include <string>
#include <iostream>

FGame::FGame()
{
	bGameFinished = false;
}

void FGame::StartGame()
{
	//Initialize the game 
	/*
	Create the player	
	*/
	
	GameDifficulty = 1;

	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << " Insert Player Name: ";
	std::string PlayerName = "";
	std::getline(std::cin, PlayerName);

	GeneratePlayer(PlayerName);

	std::map<int, Item>::iterator it;
	for (it = item_pool.begin(); it != item_pool.end(); ++it)
	{		
		Item curr_item = (Item)it->second;
		std::cout << it->first << " => " << curr_item.name << '\n';
		Player.inventory[it->first] = 1;
	}
}

void FGame::Update(FGame& game)
{
	/*
		v.0.0.2
		Check the status of the game
		Check the player action
		Check the playser status
		Check the enemy status
	*/	
	std::cout << std::endl;
	//Clear the screen hack
	//std::cout << std::string(3, '\n') << std::endl;
	//std::system("cls");
	//Check if the player is dead
	if (Player.IsDead())
	{
		EndGame();
	}
	else
	{
		if (currentStatus != nullptr)
		{
			currentStatus->Update(game);
		}
	}	
}

bool FGame::IsGameEnded() const
{
	return bGameFinished;
}

FActor FGame::GetPlayer() const
{
	return Player;
}

void FGame::PrintBattleResult(int PlayerDamage, int EnemyDamage, int Healing, int TempArmor)
{
	if (PlayerDamage == 0 && TempArmor != 0)
	{
		std::cout << " ----------------------------------- " << std::endl;
		std::cout << " Player defend." << std::endl;
		std::cout << " Player heals for " << Healing << std::endl;
		Player.SetArmor(Player.GetArmor() - TempArmor);
		Player.SetHitPoints(Player.GetHitPoints() + Healing);
	}
	else
	{
		std::cout << " ----------------------------------- " << std::endl;
		std::cout << " Player attack for " << PlayerDamage << std::endl;
	}
	//Check the Enemy status
	if (Enemy.IsDead()) {
		std::cout << " ---------- Player Won --------- " << std::endl;
		//add exp, increase level TODO test with one monster 1 level
		int ExpEarned = Enemy.GetExpReward();
		std::cout << " Exp:" << ExpEarned << std::endl;
		std::cout << " Gold:" << Enemy.GetCurrency() << std::endl;
		std::cout << std::endl;
		std::cout << std::endl;
		if (ExpEarned + Player.GetExperience() >= Player.GetNextLevelExp()) {
			std::cout << " ---------- Player Level Up --------- " << std::endl;
			Player.AddExperience(ExpEarned);
			std::cout << " ---------- Level " << Player.GetLevel() << "  --------- " << std::endl;
			//TODO find a better way to do this
			LevelUpPlayerStats();
		}
		else
		{
			Player.AddExperience(ExpEarned);
			Player.SetCurrency(Player.GetCurrency() + Enemy.GetCurrency());
		}

		//Increase Difficulty TODO Make the difficulty more random
		GameDifficulty++;
		std::cout << " ----------------------------------- " << std::endl;
	    SetCurrentStatus(FGame::States::MAIN_MENU);
	}
	else {
		std::cout << " Enemy attack for " << EnemyDamage << std::endl;
		std::cout << " ----------------------------------- " << std::endl;
		std::cout << std::endl;
		std::cout << std::endl;
	}
}

void FGame::ExitGame()
{
	/*
	v.0.0.2
	Print the final score
	*/
	std::cout << std::endl;
	std::cout << "Thanks for playing! Bye" << std::endl;
}

void FGame::EndGame()
{
	//Print the score and exit the game
	std::cout << std::endl;
	std::cout << "  You are dead !" << std::endl;
	std::cout << " Your score is: " << Player.GetLevel() << std::endl;

	std::string Restart = "";
	do
	{
		std::cout << " Do you want play again (y/n)? :";

		if (!(std::cin >> Restart))
		{
			std::cin.clear();
			std::cin.ignore(99, '\n'); //Clear the buffer for the getline
			Restart = "";
		}	
		
	} while (Restart[0] != 'y' && Restart[0] != 'n');

	if (Restart[0] == 'n') 
	{
		bGameFinished = true;
	}
	else if(Restart[0] == 'y')
	{
		Reset();
	}
}

void FGame::Reset()
{
	//Reset the game from the beginnning
	StartGame();
}

void FGame::PrintActorStatus(FActor Actor, bool Details)
{
	/*
	Printing the status
	*/
	std::cout << " " << Actor.GetDescription() << ": " << Actor.GetName() << " - Lv: " << Actor.GetLevel();
	std::cout << std::endl;
	std::cout << " HP: " << Actor.GetHitPoints() << std::endl;
	if (Actor.GetDescription() == "Player") {
		std::cout << " NextLevel: " << Actor.GetExperience() << "/" << Actor.GetNextLevelExp() << std::endl;
		std::cout << " Gold: " << Actor.GetCurrency() << std::endl;
	}
	if (Details) {
		std::cout << " -------------- Stats -------------- " << std::endl;
		std::cout << " Atk: " << Actor.GetAttack() << "    Def:" << Actor.GetArmor() << std::endl;
		std::cout << std::endl << std::endl;
	}
}

void FGame::GenerateNextEnemy()
{
	int BaseEnemyHp = 10;
	int BaseEnemyAtk = 5;
	int BaseEnemyArmor = 5;
	int BaseEnemyLevel = 1;
	uint32_t base_enemy_golds = 5;

	int EnemyLevel = BaseEnemyLevel + NumGenerator.CreateRandomNumber(0, GameDifficulty); //0 - 1
	int EnemyHp = CalculateEnemyStat(BaseEnemyHp, EnemyLevel, GameDifficulty);
	int EnemyAtk = CalculateEnemyStat(BaseEnemyAtk, EnemyLevel, GameDifficulty);
	int EnemyArmor = CalculateEnemyStat(BaseEnemyArmor, EnemyLevel, GameDifficulty);
	uint32_t enemy_golds = CalculateEnemyStat(base_enemy_golds, EnemyLevel, GameDifficulty);

	//If i only change the data for the enemy, can't i just change the values ?
	//instead of a new allocation ? does this generate a mem leak ?
	//Enemy.Attack = EnemyAtk;
	Enemy = FActor(GenerateEnemyName(), "Enemy", EnemyHp, EnemyAtk, EnemyArmor, EnemyLevel);
	Enemy.SetCurrency(enemy_golds);
}

void FGame::PrintPlayerStatus()
{
	PrintActorStatus(Player, true);
}

void FGame::PrintEnemyStatus()
{
	PrintActorStatus(Enemy, true);
}

void FGame::Battle()
{
	int PlayerDamage = 0;
	int EnemyDamage = 0;
	PlayerDamage = Player.Attack(Enemy);
	if (!Enemy.IsDead()) {
		EnemyDamage = Enemy.Attack(Player);
	}
	PrintBattleResult(PlayerDamage, EnemyDamage, 0, 0);
}

void FGame::Defend()
{
	int TempArmorBonus = 10 * Player.GetLevel();
	Player.SetArmor(Player.GetArmor() + TempArmorBonus);
	int EnemyDamage = Enemy.Attack(Player);
	int Healing = 10 * Player.GetLevel();
	PrintBattleResult(0, EnemyDamage, Healing, TempArmorBonus);
}

void FGame::LoadItems()
{
	//Static add items
	//TODO read from file
	Item i0 = {};
	i0.id = 0;
	i0.name = "first item";

	Item i1 = {};
	i1.id = 1;
	i1.name = "sec item";

	Item i2 = {};
	i2.id = 2;
	i2.name = "thr item";

	item_pool.insert(std::pair<int, Item>(i0.id, i0));
	item_pool.insert(std::pair<int, Item>(i1.id, i1));
	item_pool.insert(std::pair<int, Item>(i2.id, i2));

}

void FGame::PrintPlayerInventory()
{
	std::map<int, int>::iterator it;
	for (it = Player.inventory.begin(); it != Player.inventory.end(); ++it)
	{		
		Item curr_item = this->item_pool.at(it->first);
		std::cout << it->second << "x " << curr_item.id << " => " << curr_item.name << '\n';
	}
}

void FGame::GeneratePlayer(std::string Name)
{
	int BasePlayerHp = 100;
	int BasePlayerAtk = 10;
	int BasePlayerArmor = 10;
	int BasePlayerLevel = 1;

	//Using new random number generator
	int PlayerLevel = BasePlayerLevel + NumGenerator.CreateRandomNumber(0, GameDifficulty); //0 - GameDifficulty
	int PlayerHp = CalculateEnemyStat(BasePlayerHp, PlayerLevel, 0);
	int PlayerAtk = CalculateEnemyStat(BasePlayerAtk, PlayerLevel, 0);
	int PlayerArmor = CalculateEnemyStat(BasePlayerArmor, PlayerLevel, 0);

	Player = FActor(Name, "Player", PlayerHp, PlayerAtk, PlayerArmor, PlayerLevel);
}

int FGame::CalculateEnemyStat(int BaseValue, int EnemyLevel, int GameDifficulty)
{
	
	return BaseValue * EnemyLevel + NumGenerator.CreateRandomNumber(0,10) + GameDifficulty + 1;
}

void FGame::LevelUpPlayerStats()
{
	int PlayerLevel = Player.GetLevel();

	int PlayerHp = CalculateEnemyStat(100, PlayerLevel, 0);
	int PlayerAtk = CalculateEnemyStat(10, PlayerLevel, 0);
	int PlayerArmor = CalculateEnemyStat(10, PlayerLevel, 0);

	Player.SetHitPoints(PlayerHp);
	Player.SetAttack(PlayerAtk);
	Player.SetArmor(PlayerArmor);
}

std::string FGame::GenerateEnemyName()
{
	std::string names[4] = { "Rat", "Snake", "Boar", "Bear"};
	int id = NumGenerator.CreateRandomNumber(0, names->length()-1);
	return names[id];
}

void FGame::SetCurrentStatus(int state)
{
	if (currentStatus)
	{
		this->currentStatus->Leave();
	}

	state_list[state]->Enter();
	this->currentStatus = state_list[state];
}

