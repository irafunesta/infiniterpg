#include "FightState.h"
#include "FGame.h"

#include <iostream>

FightState::FightState()
{
}


FightState::~FightState()
{
}

void FightState::Update(FGame& game)
{
	//Print the main menu
	//Check for inputs
	std::system("cls");
	std::cout << " ---------- Player Status ---------- " << std::endl;
	game.PrintPlayerStatus();
	std::cout << " ---------- Enemy Status ---------- " << std::endl;
	game.PrintEnemyStatus();

	std::cout << " ----------------------------------- " << std::endl;
	std::cout << " -------- Select an action --------- " << std::endl;

	std::cout << "           1 - Attack                " << std::endl;
	std::cout << "           2 - Defend                " << std::endl;
	std::cout << "           3 - Run                   " << std::endl;
	std::cout << "           4 - Check                 " << std::endl;
		
	int Action = 0;
	Action = std::getchar();
	//Get the user input and expect it to be a number
	switch (Action)
	{
		case '1': //Attack
		{
			game.Battle();
			Continue();
			break;
		}
		case '2':
		{
			game.Defend();
			Continue();
			break;
		}
		case '3': 
		{
			game.SetCurrentStatus(FGame::States::MAIN_MENU);
			Continue("You escaped");
			break;
		}
		case '4': 
		{
			game.PrintEnemyStatus();
			Continue();
			break;
		}
		default:
		{
			std::cout << "Invalid option" << std::endl;
			Continue();
			break;
		}
	}
};
void FightState::Enter()
{
	std::cout << "Enter Fight" << std::endl;
	std::cout << "enemy appear" << std::endl;
	Continue();
};
void FightState::Leave() {
	//std::cout << "Leave Fight" << std::endl;
};