#ifndef _infrpg_item_
#define _infrpg_item_

#include <string>

class FActor;

class Item
{
public:

	static enum Category
	{
		OFFENSIVE,
		DEFENSIVE,
		SUPPORT,
		MATERIAL
	};

	Item();
	
	~Item();

	std::string name;
	std::string description;

	unsigned int id;
	bool usable;
	bool usable_inBattle;
	bool equippable;
	bool equipped;
	bool equip_slot;
	Category category;

	void Use(FActor& target);
};

#endif