#ifndef _infrpg_mainmenustate_h
#define _infrpg_mainmenustate_h

#include "State.h"

class FGame;

class MainMenuState :
	public State
{
public:
	MainMenuState();
	~MainMenuState();

	virtual void Update(FGame& game);
	virtual void Enter();
	virtual void Leave();
};

#endif