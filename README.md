# README #

### Infinite RPG by Guidi Simone ###
A Game where you fight a infinite series of monsters.
The game end whith your defeat, the level you have reached
is your score.

* v0.1.2

### Description ###

The game is a text based, turn based rpg for Windows.
After entering the player name, you have the options to start fighting or exit.
When in a fight, the monsters have randomized stats, and each defeated enemy
increase the difficulty of the next. The Goal is to reach the highest experience
level.

### Instruction ###

* Press the number corrisponding to the prompted value.
* In Menu mode press 1 to Figth and 2 to Exit the game
* In Fight mode
* 1 is for Attack, the enemy will attack right after.
* 2 is for Defending and healing some points. The enemy will still attack
* 3 escape the battle
* 4 check information of the enemy

### Disclaimer ###

* This is my first game on c++, so is really simple.

### Compilation ###
* Just open the VS 2015 solution, i have not used any external libs.